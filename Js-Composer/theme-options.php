<?php	
function neymar_integrateWithVC() {
/** ---------------------------------------------|
/* Begin: Home -- Revolution Silder        		|
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Revolution Slider', 'neymar'),
	'description'	=> esc_html__('Revolution Slider Section', 'neymar'),
	'base'			=> 'slider_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/revolution.png',
	'params'		=> 
	array(		
		array(
				'param_name'	=> 'content',
				'type'			=> 'textarea_html',
				'holder'		=> 'div',
				'class' 		=> '',
				'description'	=> esc_html__('(Shortcode form Revolution Slider)', 'neymar'),
				'group'   		=> esc_html__( 'Shortcode', 'neymar' ),
			),											
	)));			
/** ---------------------------------------------|
/* Begin: HOME -- About Us				 		 |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('About Us', 'neymar'),
	'description'	=> esc_html__('Company About Us', 'neymar'),
	'base'			=> 'about_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/about_us.png',
	'params'		=> 
	array(
		array(
			'type' => 'textfield',
			'value' => esc_html__('Write on your', 'neymar'),
			'heading' => esc_html__('Title', 'neymar'),
			'param_name' => 'about_title',
			"description" => __( "Ex: About Us", "neymar" ),
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#333', 'neymar'),
			'heading' => esc_html__('Color', 'neymar'),
			'param_name' => 'about_title_co',
			"description" => __( "Ex: #3345e3", "neymar" ),
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __( "Description", "neymar" ),
			"param_name" => "about_description",
			"value" => __( "Write Description ", "neymar" ),
			"description" => __( "Description for about Analysis.", "neymar" )
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#333', 'neymar'),
			'heading' => esc_html__('Color', 'neymar'),
			'param_name' => 'about_description_co',
			"description" => __( "Ex: #3345e3", "neymar" ),
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#fff', 'neymar'),
			'heading' => esc_html__('Section Background Color', 'neymar'),
			'param_name' => 'about_description_bg',
			"description" => __( "Ex: #3345e3", "neymar" ),
		),
		array(
			'type' 			=> 'param_group',
			'heading'		=> esc_html__('Analysis', 'neymar'),
			'param_name' 	=> 'about_group_item',
			'group'			=> esc_html__('Analysis Items', 'neymar'),
			// Note params is mapped inside param-group:
		   'params' 		=>
				array(															   
					array(
						"type"        => "attach_images",
						"heading"     => esc_html__( "Screenshots", "neymar" ),
						"description" => esc_html__( "Add Screenshots.", "neymar" ),
						"param_name"  => "about_group_icon",
						"value"       => '',
					),
					array(
					'type' => 'iconpicker',
					'heading' => __('Select a Icon', 'neymar'),
					'param_name' => 'about_individual_icon',
					'settings' => array(
						'emptyIcon' => true,
						'iconsPerPage' => 100,
						'type' => 'fontawesome',
						),
					),	
					array(
						'type' => 'textfield',
						'value' => esc_html__('Analysis Item Title', 'neymar'),
						'heading' => esc_html__('Title', 'neymar'),
						'param_name' => 'about_group_title',
					),
					array(
						'type' => 'colorpicker',
						'value' => esc_html__('#333', 'neymar'),
						'heading' => esc_html__('Title Color', 'neymar'),
						'param_name' => 'about_group_title_co',
					),
					array(
						'type' => 'textarea',
						'value' => esc_html__('Analysis Item Description', 'neymar'),
						'heading' => esc_html__('Description', 'neymar'),
						'param_name' => 'about_group_description',
					),
					array(
						'type' => 'colorpicker',
						'value' => esc_html__('#333', 'neymar'),
						'heading' => esc_html__('Description Color', 'neymar'),
						'param_name' => 'about_group_description_co',
					),
					array(
						'type' => 'colorpicker',
						'value' => esc_html__('#f7f004', 'neymar'),
						'heading' => esc_html__('Section hover Color', 'neymar'),
						'param_name' => 'about_group_color',
					),

				),
			),	
		)
	)
);					
/** ---------------------------------------------|
/* Begin: HOME -- OUR SERVICES			 		|
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Our Service', 'neymar'),
	'description'	=> esc_html__('Company Service', 'neymar'),
	'base'			=> 'service_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/service.png',
	'params'		=> 
	array(
		array(
			'type' 			 => 'textfield',
			'value' 		 => esc_html__('Write on your', 'neymar'),
			'heading' 		 => esc_html__('Title', 'neymar'),
			'param_name'	 => 'service_title',
			"description"	 => __( "Ex: OUR SERVICES", "neymar" ),
		),
		array(
			'type'		 => 'colorpicker',
			'value' 	 => esc_html__('#333', 'neymar'),
			'heading' 	 => esc_html__('Title Color', 'neymar'),
			'param_name' => 'service_title_co',
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __( "Description", "neymar" ),
			"param_name" => "service_description",
			"value" => __( "Write Description ", "neymar" ),
			"description" => __( "Description for about Service.", "neymar" )
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_html__('#333', 'neymar'),
			'heading' => esc_html__('Description Color', 'neymar'),
			'param_name' => 'service_description_co',
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#fff', 'neymar'),
			'heading' => esc_html__('Section Background Color', 'neymar'),
			'param_name' => 'service_description_co_bg',
			"description" => __( "Ex: #3345e3", "neymar" ),
		),
		array(
			'type' 			=> 'param_group',
			'heading'		=> esc_html__('Service', 'neymar'),
			'param_name' 	=> 'service_group_item',
			'group'			=> esc_html__('Service Items', 'neymar'),
			// Note params is mapped inside param-group:
		   'params' 		=> 
		    array(
			   array(
					"type"        => "attach_images",
					"heading"     => esc_html__( "Screenshots", "neymar" ),
					"description" => esc_html__( "Add screenshots.", "neymar" ),
					"param_name"  => "service_group_img",
					"value"       => '',
				),
				array(
					'type' => 'iconpicker',
					'heading' => __('Select a Icon', 'neymar'),
					'param_name' => 'service_group_icon',
					'settings' => array(
						'emptyIcon' => true,
						'iconsPerPage' => 100,
						'type' => 'fontawesome',
						),
					),				
				array(
					'type' => 'textfield',
					'value' => esc_html__('Service Item Title', 'neymar'),
					'heading' => esc_html__('Title', 'neymar'),
					'param_name' => 'service_group_title',
				),
				array(
					'type' => 'colorpicker',
					'value' => esc_html__('#333', 'neymar'),
					'heading' => esc_html__('Title Color', 'neymar'),
					'param_name' => 'service_group_title_co',
				),
				array(
					'type' => 'textfield',
					'value' => esc_html__('Service Extra Title', 'neymar'),
					'heading' => esc_html__('Extra Title', 'neymar'),
					'param_name' => 'service_extra_title',
				),
				array(
					'type' => 'colorpicker',
					'value' => esc_html__('#333', 'neymar'),
					'heading' => esc_html__('Extra Title Color', 'neymar'),
					'param_name' => 'service_extra_title_co',
				),
				array(
					'type' => 'textarea',
					'value' => esc_html__('Service Extra description', 'neymar'),
					'heading' => esc_html__('Description', 'neymar'),
					'param_name' => 'service_extra_description',
				),
				array(
					'type' => 'colorpicker',
					'value' => esc_html__('#333', 'neymar'),
					'heading' => esc_html__('Description Color', 'neymar'),
					'param_name' => 'service_extra_description_co',
				),		
			),
		),					
)));	
/** ---------------------------------------------|
/* Begin: HOME -- Portfolio					 				 |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Portfolio', 'neymar'),
	'description'	=> esc_html__('Latest Works', 'neymar'),
	'base'			=> 'portfolio_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/portfolio.png',
	'params'		=> 
	array(
		array(
			'type' => 'textfield',
			'value' => esc_html__('Title', 'neymar'),
			'heading' => esc_html__('Title', 'neymar'),
			'param_name' => 'portfolio_section_title',
			"description" => __( "Ex: LATEST WORKS", "neymar" ),
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_html__('#333', 'neymar'),
			'heading' => esc_html__('Title Color', 'neymar'),
			'param_name' => 'portfolio_section_title_co',
		),
		array(
			 'type' => 'textfield',
			 'value' => esc_html__('Description', 'neymar'),
			 'heading' => esc_html__('Description', 'neymar'),
			 'param_name' => 'portfolio_section_description',
			 "description" => __( "Ex: Description", "neymar" ),
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_html__('#333', 'neymar'),
			'heading' => esc_html__('Description Color', 'neymar'),
			'param_name' => 'portfolio_section_description_co',
		),
	    array(
			"type"        => "attach_images",
			"heading"     => esc_html__( "Screenshots", "neymar" ),
			"description" => esc_html__( "Add screenshots.", "neymar" ),
			"param_name"  => "portfolio_img",
			"value"       => '',
		),
		array(
			'type' => 'iconpicker',
			'heading' => __('Select a Icon', 'neymar'),
			'param_name' => 'portfolio_img_icon',
			'settings' => array(
				'emptyIcon' => true,
				'iconsPerPage' => 100,
				'type' => 'fontawesome',
				),
					),		
		array(
			'type' => 'colorpicker',
			'value' => esc_html__('#333', 'neymar'),
			'heading' => esc_html__('Slug Color', 'neymar'),
			'param_name' => 'portfolio_section_slug_co',
		),
		array(
		'type' => 'colorpicker',
		'value' => esc_attr('#fff', 'neymar'),
		'heading' => esc_html__('Section Background Color', 'neymar'),
		'param_name' => 'portfolio_section_slug_co_bg',
		"description" => __( "Ex: #3345e3", "neymar" ),
	),
)));		
/** ---------------------------------------------|
/* Begin: HOME -- Count Achievement					 				 |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Achievement', 'neymar'),
	'description'	=> esc_html__('Company Achievement Count', 'neymar'),
	'base'			=> 'count_achievement_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/achivement.png',
	'params'		=> 
	array(
		array(
			"type"        => "attach_images",
			"heading"     => esc_html__( "Background Image", "neymar" ),
			"description" => esc_html__( "Add screenshots.", "neymar" ),
			"param_name"  => "count_achievement_bg_img",
			"value"       => '',
		),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#fff', 'neymar'),
			'heading' => esc_html__('Section Background Color', 'neymar'),
			'param_name' => 'count_achievement_bg_img_co_bg',
			"description" => __( "Ex: #3345e3", "neymar" ),
		),
		array(
			'type' 			=> 'param_group',
			'heading'		=> esc_html__('Achievement', 'neymar'),
			'param_name' 	=> 'count_achievement_group',
			'group'			=> esc_html__('Achievement Item', 'neymar'),
			'params' 		=> 
			array(		
				array(
					"type"        => "attach_images",
					"heading"     => esc_html__( "Screenshots", "neymar" ),
					"description" => esc_html__( "Add screenshots.", "neymar" ),
					"param_name"  => "count_achievement_group_img",
					"value"       => '',
				),
				array(
					'type' => 'iconpicker',
					'heading' => __('Select a Icon', 'neymar'),
					'param_name' => 'count_achievement_group_icon',
					'settings' => array(
						'emptyIcon' => true,
						'iconsPerPage' => 100,
						'type' => 'fontawesome',
						),
					),	
				array(
					'type' => 'textfield',
					'value' => esc_html__('HAPPY CLIENTS', 'neymar'),
					'heading' => esc_html__('Title', 'neymar'),
					'param_name' => 'count_achievement_section_title',
					"description" => __( "Ex: HAPPY CLIENTS", "neymar" ),
				),
				array(
					'type' => 'colorpicker',
					'value' => esc_html__('#ffffff', 'neymar'),
					'heading' => esc_html__('Title Color', 'neymar'),
					'param_name' => 'count_achievement_section_title_co',
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Count Number", "neymar" ),
					"param_name" => "count_achievement_section_count",
					"value" => __( "5214", "neymar" ),
					"description" => __( "Ex: 5214.", "neymar" )
				),
				array(
					'type' => 'colorpicker',
					'value' => esc_html__('#ffffff', 'neymar'),
					'heading' => esc_html__('Count Color', 'neymar'),
					'param_name' => 'count_achievement_section_count_co',
				),
			)
		)							 
	)
)
);
/** ---------------------------------------------|
/* Begin: about_us_page -- CREATIVE TEAM         |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Team', 'neymar'),
	'description'	=> esc_html__('Team Section', 'neymar'),
	'base'			=> 'team_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/group.png',
	'params'		=> 
	array(
		array(
			'type' => 'textfield',
			'value' => esc_html__('Write on your', 'neymar'),
			'heading' => esc_html__('Title', 'neymar'),
			'param_name' => 'team_section_title',
			"description" => __( "Ex: CREATIVE TEAM", "neymar" ),
				),
		array(
			'type' => 'colorpicker',
			'value' => esc_html__('#373635', 'neymar'),
			'heading' => esc_html__('Title Color', 'neymar'),
			'param_name' => 'team_section_title_co',
			),
		array(
			'type' => 'textfield',
			'value' => esc_html__('Write on your', 'neymar'),
			'heading' => esc_html__('Description', 'neymar'),
			'param_name' => 'team_section_descr',
			"description" => __( "Ex: Description", "neymar" ),
				),
		array(
			'type' => 'colorpicker',
			'value' => esc_html__('#797979', 'neymar'),
			'heading' => esc_html__('Description Color', 'neymar'),
			'param_name' => 'team_section_descr_co',
			),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#fff', 'neymar'),
			'heading' => esc_html__('Section Background Color', 'neymar'),
			'param_name' => 'team_section_descr_co_co_bg',
			"description" => __( "Ex: #3345e3", "neymar" ),
		),
		array(
			'type' 			=> 'param_group',
			'heading'		=> esc_html__('Add Member', 'neymar'),
			'param_name' 	=> 'team_section_group',
			'group'			=> esc_html__('Members', 'neymar'),
			// Note params is mapped inside param-group:
			'params' 		=> 
			array(
				array(
					"type"        => "attach_images",
					"heading"     => esc_html__( "Screenshots", "neymar" ),
					"description" => esc_html__( "Add Screenshots.", "neymar" ),
					"param_name"  => "team_section_group_image",
					"value"       => '',
				),											
				array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Name', 'neymar'),
					'param_name' => 'team_section_group_name',
					"description" => __( "Ex: JON SMITH", "neymar" ),
						),
				array(
					'type' => 'colorpicker',
					'value' => esc_attr('#373635', 'neymar'),
					'heading' => esc_html__('Name Color', 'neymar'),
					'param_name' => 'team_section_group_name_co',
					),
				array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Designation', 'neymar'),
					'param_name' => 'team_section_group_designation',
					"description" => __( "Ex: Designer", "neymar" ),
						),
				array(
					'type' => 'colorpicker',
					'value' => esc_attr('#373635', 'neymar'),
					'heading' => esc_html__('Designation Color', 'neymar'),
					'param_name' => 'team_section_group_designation_co',
					),
				array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Twitter link', 'neymar'),
					'param_name' => 'team_section_group_tw',
					"description" => __( "Ex: www.example.com", "neymar" ),
						),
				array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Facebook link', 'neymar'),
					'param_name' => 'team_section_group_fb',
					"description" => __( "Ex: www.example.com", "neymar" ),
						),
				array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Pinterest link', 'neymar'),
					'param_name' => 'team_section_group_pin',
					"description" => __( "Ex: www.example.com", "neymar" ),
						),
				array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Thumbl link', 'neymar'),
					'param_name' => 'team_section_group_thml',
					"description" => __( "Ex: www.example.com", "neymar" ),
					),
				array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Skype link', 'neymar'),
					'param_name' => 'team_section_group_skype',
					"description" => __( "Ex: www.example.com", "neymar" ),
				),
				)),
	)));		
/** ---------------------------------------------|
/* Begin: HOME --Testimonial			 				 |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Testimonial', 'neymar'),
	'description'	=> esc_html__('Testimonial Sections', 'neymar'),
	'base'			=> 'testimonial_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/testimonial.png',
	'params'		=> 
	array(
		array(
			"type"        => "attach_images",
			"heading"     => esc_html__( "Background image", "neymar" ),
			"description" => esc_html__( "Add image.", "neymar" ),
			"param_name"  => "testimonial_section_bg_image",
			"value"       => '',
			),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#fff', 'neymar'),
			'heading' => esc_html__('Section Background Color', 'neymar'),
			'param_name' => 'testimonial_section_bg_co_bg',
			"description" => __( "Ex: #3345e3", "neymar" ),
		),			
		array(
			'type' => 'textfield',
			'value' => esc_html__('Write on your', 'neymar'),
			'heading' => esc_html__('Title', 'neymar'),
			'param_name' => 'testimonial_title',
			"description" => __( "Ex: clients says about us", "neymar" ),
			),
		array(
			'type' => 'colorpicker',
			'value' => esc_attr('#fbed56', 'neymar'),
			'heading' => esc_html__('Title Color', 'neymar'),
			'param_name' => 'testimonial_title_co',
			),
		array(
		'type' 			=> 'param_group',
		'heading'		=> esc_html__('Testimonial', 'neymar'),
		'param_name' 	=> 'testimonial_groups',
		'group'			=> esc_html__('Testimonial Items', 'neymar'),
		// Note params is mapped inside param-group:
		'params' 		=> 
		array(
			array(
				"type"        => "attach_images",
				"heading"     => esc_html__( "Photo", "neymar" ),
				"description" => esc_html__( "Add Photo.", "neymar" ),
				"param_name"  => "testimonial_groups_image",
				"value"       => '',
			),
			array(
					'type' => 'textfield',
					'value' => esc_html__('Write on your', 'neymar'),
					'heading' => esc_html__('Name', 'neymar'),
					'param_name' => 'testimonial_groups_title',
					"description" => __( "Ex: TONOY ANDERSON", "neymar" ),
				),
			array(
				'type' => 'colorpicker',
				'value' => esc_attr('#dcdcdc', 'neymar'),
				'heading' => esc_html__('Name Color', 'neymar'),
				'param_name' => 'testimonial_groups_title_co',
				),
			array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Designation", "neymar" ),
					"param_name" => "testimonial_groups_designation",
					"value" => __( "Managing Director", "neymar" ),
					"description" => __( "Ex: Managing Director", "neymar" )
				 ),
			array(
				'type' => 'colorpicker',
				'value' => esc_attr('#d0d0d0', 'neymar'),
				'heading' => esc_html__('Designation Color', 'neymar'),
				'param_name' => 'testimonial_groups_designation_co',
				),
			array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Description", "neymar" ),
					"param_name" => "testimonial_groups_description",
					"value" => __( "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "neymar" ),
					"description" => __( "Write Description", "neymar" )
				 ),
			array(
				'type' => 'colorpicker',
				'value' => esc_attr('#d0d0d0', 'neymar'),
				'heading' => esc_html__('Description Color', 'neymar'),
				'param_name' => 'testimonial_groups_description_co',
				),
),),)));
/** ---------------------------------------------|
/* Begin: HOME -- Pricing			 		 |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Pricing', 'neymar'),
	'description'	=> esc_html__('Pricing Sections', 'neymar'),
	'base'			=> 'pricing_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/pricing.png',
	'params'		=> array(
						array(
							'type' => 'textfield',
							'value' => esc_html__('Write on your', 'neymar'),
							'heading' => esc_html__('Title', 'neymar'),
							'param_name' => 'pricing_title',
							"description" => __( "Ex: Pricing", "neymar" ),
						),
						array(
							'type' => 'colorpicker',
							'value' => esc_attr('#373635', 'neymar'),
							'heading' => esc_html__('Title Color', 'neymar'),
							'param_name' => 'pricing_title_co',
							),
						array(
							'type' => 'textfield',
							'value' => esc_html__('Write on your', 'neymar'),
							'heading' => esc_html__('Description', 'neymar'),
							'param_name' => 'pricing_description',
							"description" => __( "Description about Section", "neymar" ),
						),
						array(
							'type' => 'colorpicker',
							'value' => esc_attr('#797979', 'neymar'),
							'heading' => esc_html__('Description Color', 'neymar'),
							'param_name' => 'pricing_description_co',
							),
						array(
							'type' => 'colorpicker',
							'value' => esc_attr('#fff', 'neymar'),
							'heading' => esc_html__('Section Background Color', 'neymar'),
							'param_name' => 'pricing_description_co_bg',
							"description" => __( "Ex: #3345e3", "neymar" ),
						),
	
						array(
							'type' 			=> 'param_group',
							'heading'		=> esc_html__('Pricing', 'neymar'),
							'param_name' 	=> 'pricing_groups',
							'group'			=> esc_html__('Pricing Items', 'neymar'),
						// Note params is mapped inside param-group:
						   'params' 	=> 
							array(
							   array(
								'type' => 'textfield',
								'value' => esc_html__('Write on your', 'neymar'),
								'heading' => esc_html__('Title', 'neymar'),
								'param_name' => 'pricing_groups_title',
								"description" => __( " ", "neymar" ),
								),
								array(
									'type' => 'colorpicker',
									'value' => esc_attr('#373635', 'neymar'),
									'heading' => esc_html__('Title Color', 'neymar'),
									'param_name' => 'pricing_groups_title_co',
									),
								array(
									'type' => 'textfield',
									'value' => esc_html__('$', 'neymar'),
									'heading' => esc_html__('Currency', 'neymar'),
									'param_name' => 'pricing_groups_doller_currency',
									"description" => __( " ", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Price', 'neymar'),
									'param_name' => 'pricing_groups_doller',
									"description" => __( " ", "neymar" ),
								),
								array(
									'type' => 'colorpicker',
									'value' => esc_attr('#373635', 'neymar'),
									'heading' => esc_html__('Doller Color', 'neymar'),
									'param_name' => 'pricing_groups_doller_co',
									),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Duration', 'neymar'),
									'param_name' => 'pricing_groups_duration',
									"description" => __( "Description about Section", "neymar" ),
								),
								array(
									'type' => 'colorpicker',
									'value' => esc_attr('#666666', 'neymar'),
									'heading' => esc_html__('Other Color', 'neymar'),
									'param_name' => 'pricing_groups_duration_co',
									),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label One', 'neymar'),
									'param_name' => 'pricing_groups_one',
									"description" => __( "Total Users", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label Two', 'neymar'),
									'param_name' => 'pricing_groups_two',
									"description" => __( "Unlimited Styles", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label Three', 'neymar'),
									'param_name' => 'pricing_groups_three',
									"description" => __( "Advance Protection", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label Four', 'neymar'),
									'param_name' => 'pricing_groups_four',
									"description" => __( "Cloud Storage", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label Five', 'neymar'),
									'param_name' => 'pricing_groups_five',
									"description" => __( "24/7 Customer Service", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label Six', 'neymar'),
									'param_name' => 'pricing_groups_six',
									"description" => __( "Backup Service", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Buy Now Link', 'neymar'),
									'param_name' => 'pricing_groups_link',
									"description" => __( "www.link.com", "neymar" ),
								),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Buy Now', 'neymar'),
									'heading' => esc_html__('Buy Now Button', 'neymar'),
									'param_name' => 'pricing_groups_text',
									"description" => __( "Buy Now", "neymar" ),
								),
		),),
	)));
/** ---------------------------------------------|
/* Begin: HOME -- LATEST BLOG		 			 |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Latest Blog', 'neymar'),
	'description'	=> esc_html__('Latest Blog Sections', 'neymar'),
	'base'			=> 'latest_blog_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/latest.png',
	'params'		=> array(
						array(
							'type' => 'textfield',
							'value' => esc_html__('Write on your', 'neymar'),
							'heading' => esc_html__('Title', 'neymar'),
							'param_name' => 'latest_blog_title',
							"description" => __( "Ex: LATEST BLOG", "neymar" ),
						),
						array(
							'type' => 'colorpicker',
							'value' => esc_attr('#373635', 'neymar'),
							'heading' => esc_html__('Title Color', 'neymar'),
							'param_name' => 'latest_blog_title_co',
							),
						array(
							'type' => 'textfield',
							'value' => esc_html__('Write on your', 'neymar'),
							'heading' => esc_html__('Description', 'neymar'),
							'param_name' => 'latest_blog_desciption',
							"description" => __( "Description", "neymar" ),
						),
						array(
							'type' => 'colorpicker',
							'value' => esc_attr('#797979', 'neymar'),
							'heading' => esc_html__('Description Color', 'neymar'),
							'param_name' => 'latest_blog_desciption_co',
							),
							array(
								'type' => 'textfield',
								'value' => esc_html__('3', 'neymar'),
								'heading' => esc_html__('Post Per Page', 'neymar'),
								'param_name' => 'latest_blog_number',
								"description" => __( "Ex: 3", "neymar" ),
							),
							array(
								'type' => 'textfield',
								'value' => esc_html__('View More', 'neymar'),
								'heading' => esc_html__('View More', 'neymar'),
								'param_name' => 'latest_blog_view_more',
								"description" => __( "Ex: View More", "neymar" ),
							),
							array(
								'type' => 'textfield',
								'value' => esc_html__(' ', 'neymar'),
								'heading' => esc_html__('Link', 'neymar'),
								'param_name' => 'latest_blog_view_more_link',
								"description" => __( "Ex: www.yoursite.com/blog", "neymar" ),
							),
							array(
							'type' => 'colorpicker',
							'value' => esc_attr('#fbed56', 'neymar'),
							'heading' => esc_html__('Button Color', 'neymar'),
							'param_name' => 'latest_blog_view_more_color',
							),
							array(
								'type' => 'colorpicker',
								'value' => esc_attr('#fff', 'neymar'),
								'heading' => esc_html__('Section Background Color', 'neymar'),
								'param_name' => 'latest_blog_view_more_color_co_bg',
								"description" => __( "Ex: #3345e3", "neymar" ),
							),

	)));
/** ---------------------------------------------|
/* Begin: CONTACT US  	 			 |
/** ---------------------------------------------*/
vc_map(array(
	'name'			=> esc_html__('Contact', 'neymar'),
	'description'	=> esc_html__('Contact', 'neymar'),
	'base'			=> 'contact_us_section',
	'category'		=> esc_html__('Neymar', 'neymar'),
	'icon'			=> get_template_directory_uri(). '/assets/images/vc/contact_us.png',
	'params'		=> array(
							array(
							'type' => 'textfield',
							'value' => esc_html__('Write on your', 'neymar'),
							'heading' => esc_html__('Title', 'neymar'),
							'param_name' => 'contact_us_label',
							"description" => __( "Ex: Contact", "neymar" ),
							),
							array(
								'type' => 'colorpicker',
								'value' => esc_attr('#373635', 'neymar'),
								'heading' => esc_html__('Title Color', 'neymar'),
								'param_name' => 'contact_us_label_co',
								),
							array(
								'type' => 'textfield',
								'value' => esc_html__('Write on your', 'neymar'),
								'heading' => esc_html__('Description', 'neymar'),
								'param_name' => 'contact_us_description',
								"description" => __( "Ex: Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod ", "neymar" ),
							),
							array(
								'type' => 'colorpicker',
								'value' => esc_attr('#797979', 'neymar'),
								'heading' => esc_html__('Description Color', 'neymar'),
								'param_name' => 'contact_us_description_co',
								),
							array(
								'type' => 'colorpicker',
								'value' => esc_attr('#fff', 'neymar'),
								'heading' => esc_html__('Section Background Color', 'neymar'),
								'param_name' => 'contact_us_description_co_co_bg',
								"description" => __( "Ex: #3345e3", "neymar" ),
							),

							array(
							'type' 			=> 'param_group',
							'heading'		=> esc_html__('Contact', 'neymar'),
							'param_name' 	=> 'contact_groups',
							'group'			=> esc_html__('Contact Items', 'neymar'),
							// Note params is mapped inside param-group:
						   'params' 		=> 
						    array(
								array(
									"type"        => "attach_images",
									"heading"     => esc_html__( "Logo", "neymar" ),
									"description" => esc_html__( "Add Logo.", "neymar" ),
									"param_name"  => "contact_groups_img",
									"value"       => '',
								),
								array(
									'type' => 'iconpicker',
									'heading' => __('Select a Icon', 'neymar'),
									'param_name' => 'contact_groups_icon',
									'settings' => array(
										'emptyIcon' => true,
										'iconsPerPage' => 100,
										'type' => 'fontawesome',
										),
											),		
							   array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label', 'neymar'),
									'param_name' => 'contact_groups_title',
									"description" => __( " ", "neymar" ),
								),
								array(
									'type' => 'colorpicker',
									'value' => esc_attr('#373635', 'neymar'),
									'heading' => esc_html__('Label One Color', 'neymar'),
									'param_name' => 'contact_groups_title_co',
									),
								array(
									'type' => 'textfield',
									'value' => esc_html__('Write on your', 'neymar'),
									'heading' => esc_html__('Label Two', 'neymar'),
									'param_name' => 'contact_groups_title2',
									"description" => __( " ", "neymar" ),
								),
								array(
									'type' => 'colorpicker',
									'value' => esc_attr('#373635', 'neymar'),
									'heading' => esc_html__('Label Two Color', 'neymar'),
									'param_name' => 'contact_groups_title2_co',
									),
									),),
								array(
									'param_name'	=> 'content',
									'type'			=> 'textarea_html',
									'holder'		=> 'div',
									'class' 		=> '',
									'description'	=> esc_html__('(Shortcode form Contact form)', 'neymar'),
									'group'   		=> esc_html__( 'Form Shortcode', 'neymar' ),
									),											
/*===================gmap=======================*/									
							array(
								'param_name'	=> 'map_center_lat',
								'type'			=> 'textfield',
								'heading'		=> __( '(It should be \'Country Name\' of center/main branch.)<br><br>Latitude', 'neymar' ),
								'group'			=> __( 'Map Center', 'neymar' ),
								'value'			=> 1.3581617,
								),
							array(
								'param_name'	=> 'map_center_lan',
								'type'			=> 'textfield',
								'heading'		=> __( 'Longitude', 'neymar' ),
								'group'			=> __( 'Map Center', 'neymar' ),
								'value'			=> 103.9090659,
								),
							array(
								'param_name'	=> 'map_zoom',
								'type'			=> 'textfield',
								'heading'		=> __( 'Map Zoom Position', 'neymar' ),
								'group'			=> __( 'Map Center', 'neymar' ),
								'value'			=> 13,
								),
							// params group
							array(
								'type' => 'param_group',
								'value' => '',
								'heading'		=> __( 'Add New Branch', 'neymar' ),
								'param_name' => 'branch_group_group',
								'group'			=> __( 'Add Branches', 'neymar' ),
								'params' => 
									array(									
										array(
											'param_name'	=> 'google_branch_title',
											'type'			=> 'textfield',
											'heading'		=> __( 'Place Name', 'neymar' ),
											'value'			=> 'Paya Lebar Air Base (QPG)',
											),
										array(
											'param_name'	=> 'google_branch_lat',
											'type'			=> 'textfield',
											'heading'		=> __( 'Latitude', 'neymar' ),
											'value'			=> '23.769223',
											),
										array(
											'param_name'	=> 'google_branch_lan',
											'type'			=> 'textfield',
											'heading'		=> __( 'Longitude', 'neymar' ),
											'value'			=> '90.371346',
											),
										array(
											'param_name'	=> 'google_branch_icon_image',
											'type'			=> 'attach_image',
											'heading'		=> __( 'Location Indicator', 'neymar' ),
											)
								)
							),
							//end group
							array(
								'param_name'	=> 'administrative_color',
								'type'			=> 'colorpicker',
								'value'			=> '#f0f0f0',
								'heading'		=> __( 'Administrative Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'poi_color',
								'type'			=> 'colorpicker',
								'value'			=> '#f0f0f0',
								'heading'		=> __( 'pointer Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_text_color',
								'type'			=> 'colorpicker',
								'value'			=> '#262626',
								'heading'		=> __( 'Text Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_water_color',
								'type'			=> 'colorpicker',
								'value'			=> '#a0a09a',
								'heading'		=> __( 'Water Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_landscape_color',
								'type'			=> 'colorpicker',
								'value'			=> '#ebebeb',
								'heading'		=> __( 'Landscape Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_highway_color',
								'type'			=> 'colorpicker',
								'value'			=> '#ebebeb',
								'heading'		=> __( 'Highway Road Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),	
							array(
								'param_name'	=> 'arterial_road_color',
								'type'			=> 'colorpicker',
								'value'			=> '#ebebeb',
								'heading'		=> __( 'Arterial Road Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_localroad_color',
								'type'			=> 'colorpicker',
								'value'			=> '#f4f4f4',
								'heading'		=> __( 'Local Road Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_park_color',
								'type'			=> 'colorpicker',
								'value'			=> '#ebebeb',
								'heading'		=> __( 'Park/Lake Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_transit_color',
								'type'			=> 'colorpicker',
								'value'			=> '#ebebeb',
								'heading'		=> __( 'Transit Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
							array(
								'param_name'	=> 'map_geo_stroke_color',
								'type'			=> 'colorpicker',
								'value'			=> '#ebebeb',
								'heading'		=> __( 'Geometry Stroke Color', 'neymar' ),
								'group'			=> __( 'Color Changing Options', 'neymar' ),
								),
)));	
//===================================END=============================================	
}
add_action( 'vc_before_init', 'neymar_integrateWithVC' );	

